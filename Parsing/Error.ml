type t =
  | Illegal_character of char
  | Illegal_escape_char
  | Unterminated_string
  | Unterminated_comment
  | Syntax
  | Type
  | Unknown_method
  | Unmatching_args
  | Bad_cond_type
  | Types_not_matching
  | Unknown_var
  | Unknown_type
  | Already_defined
  | Circular_class_def
  | Empty_option

exception Error of t * Location.t;;
exception DupError of string;;
exception CircularClassDef of string;;
exception Bad_method_redefinition of string;;

(* Les erreurs. *)
let report_error = function
  | Illegal_character c ->
      print_string "Illegal character (";
      print_char c;
      print_string "): "
  | Illegal_escape_char ->
      print_endline "Illegal escape character in string: "
  | Unterminated_string ->
      print_endline "String literal not terminated: "
  | Unterminated_comment ->
      print_endline "Comment not terminated: "
  | Syntax ->
      print_endline "Syntax error: "
  | Type ->
      print_endline "Type error: "
  | Unknown_method ->
      print_endline "Unknown method: "
  | Unmatching_args ->
      print_endline "Arguments not matching: "
  | Bad_cond_type ->
      print_endline "Condition must be a Boolean: "
  | Types_not_matching ->
      print_endline "Wrong type relation: "
  | Unknown_var ->
      print_endline "Unknown var: "
  | Unknown_type ->
      print_endline "Unknown type: "
  | Already_defined ->
      print_endline "Already defined: "
  | Empty_option ->
      print_endline "Empty option: "

let illegal_char char loc =
  raise (Error(Illegal_character char, loc))

let illegal_escape_char loc =
  raise (Error(Illegal_escape_char, loc))

let unterminated_string loc =
  raise (Error (Unterminated_string, loc))

let unterminated_comment loc =
  raise (Error (Unterminated_comment, loc))

let syntax loc =
  raise (Error (Syntax, loc))

let typee loc =
  raise (Error (Type, loc))

let unknown_method loc =
    raise (Error (Unknown_method, loc))

let unmatching_args loc =
    raise (Error (Unmatching_args, loc))

let bad_cond_type loc =
    raise (Error (Bad_cond_type, loc))

let types_not_matching loc =
    raise (Error (Types_not_matching, loc))

let unknown_type loc =
    raise (Error (Unknown_type, loc))

let unknown_var loc =
    raise (Error (Unknown_var, loc))

let already_defined loc =
    raise (Error (Already_defined, loc))

let empty_option loc =
    raise (Error (Empty_option, loc))

let duplicated_class classe =
  raise (DupError(classe))

let circular_class_def classe = 
  raise (CircularClassDef(classe))

let bad_method_redefinition m =
  raise (Bad_method_redefinition(m))
