(* The error type with its associated exception for the compiler *)
type t
exception Error of t * Location.t
exception DupError of string
exception CircularClassDef of string
exception Bad_method_redefinition of string

(* print an error *)
val report_error : t -> unit

(* raise the various errors *)
val illegal_char : char -> Location.t -> 'a
val illegal_escape_char : Location.t -> 'a
val unterminated_string : Location.t -> 'a
val unterminated_comment : Location.t -> 'a
val syntax : Location.t -> 'a
val typee : Location.t -> 'a
val circular_class_def : string -> 'a
val duplicated_class : string -> 'a
val bad_method_redefinition : string -> 'a
val unknown_method : Location.t -> 'a
val unknown_var : Location.t -> 'a
val unknown_type : Location.t -> 'a
val unmatching_args : Location.t -> 'a
val bad_cond_type : Location.t -> 'a
val types_not_matching : Location.t -> 'a
val already_defined : Location.t -> 'a
val empty_option : Location.t -> 'a
