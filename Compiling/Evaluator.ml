open Compiler
open List
open AST
open Typer
open Located

type pointer = int
type heap_attribute =
    | Int of int
    | String of string
    | Boolean of bool
    | Ref of pointer
    | Null


let get getMEEEEEEEE = match getMEEEEEEEE with
  | None -> raise Not_found
  | Some(a) -> a

type heap_content = (string, heap_attribute) Hashtbl.t
type heap_type = {
    mutable hcounter: pointer;
    hcontent: (int, heap_content) Hashtbl.t;
}
type eval_context = heap_content

(* a container object for all the elements we need in eval *)
type evaluator_context =
    {
        eheap: heap_type;
        econtext: eval_context;
        eclass_tbl: classes_tbl;
        emethods_tbl: methods_tbl_t;
        eattr_tbl: attr_tbl;
        mutable ethis: pointer list;
        esummary_tbl: astabstract;
    }


let string_of_attr = function
    | Int i -> string_of_int i
    | String s -> s
    | Boolean b -> string_of_bool b
    | Ref r -> string_of_int r
    | Null -> "Null"

(*
let print_heap heap =
    let content = heap.hcontent in
    Hashtbl.iter
    (fun a b -> print_endline (a ^ ": ") content; print_ref eval_context ) 
    *)

let create_heap = { hcounter = 0; hcontent = Hashtbl.create 500 }
let create_context = Hashtbl.create 40

let create_eval_context methods_tbl attr_tbl classes_tbl summary_table =
    {
        eheap = create_heap;
        econtext = create_context;
        eclass_tbl = classes_tbl;
        emethods_tbl = methods_tbl;
        eattr_tbl = attr_tbl;
        ethis = [];
        esummary_tbl = summary_table;
    }

let get_this eval_context =
    Hashtbl.find eval_context.eheap.hcontent (hd eval_context.ethis)

let add_to_context context name value =
    Hashtbl.add context name value

let rm_from_context context name =
    Hashtbl.remove context name

let get_this_ref eval_context = hd eval_context.ethis

let set_this eval_context pointer =
    eval_context.ethis <- pointer::(eval_context.ethis)

let pop_this eval_context =
    eval_context.ethis <- (tl eval_context.ethis)

let add_in_heap heap value =
    Hashtbl.add heap.hcontent heap.hcounter value;
    let res = heap.hcounter in heap.hcounter <- (heap.hcounter + 1);
    Ref(res)

let bool_of_Boolean = function
    Boolean b -> b

let string_of_String = function
    String b -> b

let int_of_Int = function
    Int b -> b


let get_bool_from_heap heap pointer =
    let container = Hashtbl.find heap.hcontent pointer in
    bool_of_Boolean (Hashtbl.find container "value")

let get_string_from_heap heap pointer =
    let container = Hashtbl.find heap.hcontent pointer in
    string_of_String (Hashtbl.find container "value")

let get_int_from_heap heap pointer =
    let container = Hashtbl.find heap.hcontent pointer in
    int_of_Int (Hashtbl.find container "value")

let create_simple_container heap v =
    let container = Hashtbl.create 1
    in Hashtbl.add container "value" v;
    add_in_heap heap container

let create_empty_container heap =
    let container = Hashtbl.create 0
    in add_in_heap heap container

let rec pointer_of_ref eval_context i =
    try match i with
    Ref r -> r
    with Match_failure(_, _, _) ->
        pointer_of_ref eval_context
        (create_simple_container eval_context.eheap i)

let get_pointer_from_context eval_context name =
    try
        pointer_of_ref eval_context (Hashtbl.find eval_context.econtext name)
    with Not_found ->
        try
        pointer_of_ref eval_context (Hashtbl.find (get_this eval_context) name)
        with Not_found -> print_endline (name ^ " not in context neither in this"); 0

let create_value heap = function
    AST.String s -> create_simple_container heap (String s)
  | AST.Int i -> create_simple_container heap (Int i)
  | AST.Null -> create_empty_container heap
  | AST.Boolean b -> create_simple_container heap (Boolean b)

(* Change the value in the context, looking first in current context, then in
* surrounding object *)
let assign_value eval_context var_name value =
    let in_c = Hashtbl.mem eval_context.econtext var_name in
    match in_c with true -> Hashtbl.replace eval_context.econtext var_name value
    (* grâce à la phase de typeur on sait que l'objet est alors dans this *)
    | false -> Hashtbl.replace (get_this eval_context) var_name value


(* Renvoie un pointeur vers un élément du tas *)
let rec create_new_object eval_context cname =
    let new_content = Hashtbl.create 10
    and (m_tbl, a_tbl) = Hashtbl.find eval_context.eclass_tbl cname
    in Hashtbl.iter
	    ( fun a b -> 
            try 
	    	let (t, expro) = Hashtbl.find eval_context.eattr_tbl b in
	    	match expro with
                Some (expr) -> Hashtbl.add new_content a (eval eval_context expr)
              | None -> Hashtbl.add new_content a (create_empty_container eval_context.eheap)
            with Not_found -> 
                print_endline "not found in create_new_object"
	    )
	    a_tbl;
    add_in_heap eval_context.eheap new_content

(* process the args of the function to add them to the context *)
and add_args_to_context eval_context names = function
    [] -> ()
  | expr::tail ->
          add_to_context eval_context.econtext (hd names) (eval eval_context expr);
          add_args_to_context eval_context (tl names) tail          

and call_fun eval_context caller fun_hash args = 
    print_endline ("call method: " ^ fun_hash);
    match fun_hash with
    s when Str.string_match (Str.regexp "^Int_") s 0 ->
        call_int_method eval_context caller fun_hash args
  | s when Str.string_match (Str.regexp "^Boolean_") s 0 ->
        call_boolean_method eval_context caller fun_hash args
  | s when Str.string_match (Str.regexp "^String_") s 0 ->
        call_string_method eval_context caller fun_hash args
  | _ ->
    let pointer = pointer_of_ref eval_context caller
    in set_this eval_context pointer;
    let fun_summary = Hashtbl.find eval_context.emethods_tbl fun_hash
    in let fun_args_names = snd fun_summary
    in
        add_args_to_context eval_context fun_args_names args;
        eval eval_context (fst fun_summary)

and call_int_method eval_context caller fun_hash args =
    let this_val = get_int_from_heap eval_context.eheap (pointer_of_ref
    eval_context caller)
    in match fun_hash with
    | "Int neg" -> create_value eval_context.eheap (Int (0 - this_val))
    | hash -> 
            let other_val = get_int_from_heap eval_context.eheap (pointer_of_ref
                                    eval_context(eval eval_context (hd args)))
    in begin match fun_hash with
      "Int_add" -> create_value eval_context.eheap (Int (this_val + other_val))
    | "Int_sub" -> create_value eval_context.eheap (Int (this_val - other_val))
    | "Int_mul" -> create_value eval_context.eheap (Int (this_val * other_val))
    | "Int_div" -> create_value eval_context.eheap (Int (this_val / other_val))
    | "Int_mod" -> create_value eval_context.eheap (Int (this_val mod other_val))
    | "Int_gt" -> create_value eval_context.eheap (Boolean (this_val > other_val))
    | "Int_ge" -> create_value eval_context.eheap (Boolean (this_val >= other_val))
    | "Int_lt" -> create_value eval_context.eheap (Boolean (this_val < other_val))
    | "Int_le" -> create_value eval_context.eheap (Boolean (this_val <= other_val))
    | "Int_eq" -> create_value eval_context.eheap (Boolean (this_val == other_val))
    | "Int_neq" -> create_value eval_context.eheap (Boolean (this_val <> other_val))
    end

and call_boolean_method eval_context caller fun_hash args =
    let this_val = get_bool_from_heap eval_context.eheap (pointer_of_ref
    eval_context caller)
    in match fun_hash with
    | "Boolean_not" -> create_value eval_context.eheap (Boolean (not this_val))
    | hash -> 
            let other_val = get_bool_from_heap eval_context.eheap (pointer_of_ref
                                eval_context (eval eval_context (hd args)))
    in begin match fun_hash with
      "Boolean_eq" -> create_value eval_context.eheap (Boolean (this_val == other_val))
    | "Boolean_neq" -> create_value eval_context.eheap (Boolean (this_val <> other_val))
    | "Boolean_or" -> create_value eval_context.eheap (Boolean (this_val || other_val))
    | "Boolean_and" -> create_value eval_context.eheap (Boolean (this_val && other_val))
    end

and call_string_method eval_context caller fun_hash args =
    let this_val = get_string_from_heap eval_context.eheap (pointer_of_ref
    eval_context caller) in
    match fun_hash with
    | "String_print" -> create_value eval_context.eheap (print_endline this_val; String this_val)
    | hash -> 
            let other_val = get_string_from_heap eval_context.eheap (pointer_of_ref
                                        eval_context (eval eval_context (hd args)))
    in begin match fun_hash with
      "String_eq" -> create_value eval_context.eheap (Boolean (this_val == other_val))
    | "String_neq" -> create_value eval_context.eheap (Boolean (this_val <> other_val))
    | "String_add" -> create_value eval_context.eheap (String (this_val ^ other_val))
    end

(* eval the expr and return a pointer to the result value in the heap *)
and eval eval_context expr = match expr.edesc with
    New(a) -> create_new_object eval_context (Type.stringOf (elem_of a))
  | Seq(e1, e2) -> eval eval_context e1; eval eval_context e2
  | Call(ex, mname, args) -> let caller = eval eval_context ex 
        and cl_summary_mthd = fst
            (Hashtbl.find eval_context.eclass_tbl (Type.stringOf (get ex.etype)))
        in let fun_hash = Hashtbl.find cl_summary_mthd mname
        in call_fun eval_context caller fun_hash args
  | If(cond, e1, e2) ->
                let test_res = get_bool_from_heap eval_context.eheap
                (pointer_of_ref eval_context (eval eval_context cond))
                in
                begin match test_res with
                true -> eval eval_context e1
                | false -> eval eval_context e2
                end

 | Val v -> create_value eval_context.eheap v
 | Var v -> Ref(get_pointer_from_context eval_context v)
 | Assign(varname, aexpr) -> let res = eval eval_context aexpr
                in assign_value eval_context varname res; res
 | Define(name, ldtype, einit, ebody) ->
                let pinit = eval eval_context einit in
                add_to_context eval_context.econtext name pinit;
                let res = eval eval_context ebody in
                rm_from_context eval_context.econtext name; res
 | Cast(lctype, mexpr) -> eval eval_context mexpr
 | Instanceof (iexpr, litype) ->
                eval eval_context iexpr;
                let expr_type = get iexpr.etype in
                let b =is_parent eval_context.esummary_tbl (elem_of litype) expr_type in
                create_value eval_context.eheap (Boolean(b))


let print_ref eval_context ref =
    Hashtbl.iter
    (fun a b -> print_endline (a ^ ": " ^ (string_of_attr b)))
    (Hashtbl.find eval_context.eheap.hcontent (pointer_of_ref eval_context ref))

let doEval ((classes, expro) as ast) summary_table =
    let ast_summary = make_ast_summary ast in 
    let (methods_tbl, attr_tbl) = build_members_tbl ast ast_summary
    and classes_tbl = build_class_descriptor ast_summary
    in let eval_context = create_eval_context methods_tbl attr_tbl classes_tbl summary_table
    in match expro with
        Some(expr) -> print_ref eval_context (eval eval_context expr)
        (*print_heap eval_context.heap*)
      | None -> print_endline "Aucune expression à exécuter"
