open List
open Error
open AST
open Typer
open Located

(* classeNom => methode body, list des noms des arguments *)
type methods_tbl_t = (string, expression * string list) Hashtbl.t
type attr_tbl = (string, Type.t * expression option) Hashtbl.t
(* classe => methodes, attributs *)
type classes_tbl = (
                      string, 
                      ((string, string) Hashtbl.t) * ((string, string) Hashtbl.t)
                    ) Hashtbl.t

let mk_method_hash cls name = cls ^ "_" ^ name

let rec get_args_names = function
    [] -> []
  | a::tail -> (fst a)::(get_args_names tail)

(* build the tables of the methods and attributes in the className_memberName *)
let build_members_tbl ((clist, e) as ast) ast_summary = 
    let m_tbl = Hashtbl.create 500 in
    let a_tbl = Hashtbl.create 500 in
	let rec add_methods classe = function
		| [] -> ()
		| h::t -> Hashtbl.replace 
                    m_tbl 
                    (mk_method_hash classe.cname h.mname) 
                    (h.mbody, get_args_names h.margstype); 
                  add_methods classe t
    and add_attr classe = function
        | [] -> ()
        | h::t -> Hashtbl.replace 
                    a_tbl 
                    (mk_method_hash classe.cname h.aname) 
                    (elem_of h.atype, h.adefault); 
                  add_attr classe t 
	in
	let rec add_classes = function
		| [] -> ()
		| h::t -> add_methods h h.cmethods; add_attr h h.cattributes; add_classes t
	in add_classes clist; (m_tbl, a_tbl)



let collect_members cname methods_tbl source_tbl =
    let res = Hashtbl.create 10 in
    let copy_m = fun a b ->
        let hash = (mk_method_hash cname a)
        in Hashtbl.add res a hash
    in Hashtbl.iter copy_m source_tbl; res


 (* build a table of methods and attributes for each class *)
let collect_data_from_summary summary_tbl cl_tbl = 
    let methods_tbl = Hashtbl.create 100 in
    let attr_tbl = Hashtbl.create 100 in
    Hashtbl.iter
    (
        fun a b ->
            let cname = (Type.stringOf a) in
            Hashtbl.add cl_tbl cname (
                (collect_members cname methods_tbl b.cmethods), 
                (collect_members cname attr_tbl b.cattr)
            )
    ) summary_tbl

let print_methods methods_tbl = Hashtbl.iter
    (fun a b -> print_endline a) methods_tbl

let print_methods_pointers tbl = Hashtbl.iter
    (fun a b -> print_endline (a ^ ": " ^ b)) tbl

let print_accessible_methods tbl = Hashtbl.iter
    (fun a (t1, t2) -> 
        print_endline ""; 
        print_endline a;
        print_methods_pointers t1;
        print_methods_pointers t2;
    ) tbl

(* for a given class, check against its parent to get inherited classes *)
let rec collect_inherited summary cls_tbl = function
    cname when cname = "Object" -> Hashtbl.replace 
                                        cls_tbl 
                                        "Object" 
                                        ((Hashtbl.create 0), (Hashtbl.create 0) )
  | cname -> 
    let my_summary = Hashtbl.find summary (Type.fromString cname) in
    collect_inherited summary cls_tbl (Type.stringOf my_summary.cparent);
    (* Object not found *)
    let (m_tbl_parent, a_tbl_parent) = Hashtbl.find cls_tbl (Type.stringOf my_summary.cparent)
    and (m_tbl, a_tbl) = Hashtbl.find cls_tbl cname in
    let loop_over_methods a b =
        begin match a with
        mname when not (Hashtbl.mem m_tbl mname) ->
            Hashtbl.replace m_tbl mname b
        | mname -> ()
        end
    in 
    Hashtbl.iter loop_over_methods m_tbl_parent; 
    let loop_over_attributes a b =
        begin match a with
        mname when not (Hashtbl.mem a_tbl mname) ->
            Hashtbl.replace a_tbl mname b
        | mname -> ()
        end
    in 
    Hashtbl.iter loop_over_attributes a_tbl_parent; 
    Hashtbl.replace cls_tbl cname (m_tbl, a_tbl)

let build_class_descriptor summary_tbl =
    let cls_tbl = Hashtbl.create 500 in
    collect_data_from_summary summary_tbl cls_tbl;
    let class_handler a b = match a with
        cname when cname = Type.fromString "Object" -> ()
        | cname -> collect_inherited summary_tbl cls_tbl (Type.stringOf cname)
    in Hashtbl.iter class_handler summary_tbl; cls_tbl 


let compile typed_ast =
    let ast_summary = make_ast_summary typed_ast in 
    let (methods_tbl, attr_tbl) = build_members_tbl typed_ast ast_summary
    in
    print_methods methods_tbl;
    print_endline "============================================================";
    let classes_descriptor = build_class_descriptor ast_summary
    in print_accessible_methods classes_descriptor;
    print_endline "===================================="
