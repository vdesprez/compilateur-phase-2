open Error
open List

type t = string

type func = t list * t

let stringOf t = t

let fromString t = t

let create_func t = t


let rec string_of_list = function
  | [] -> ""
  | [t] -> t
  | t::l -> t^"*"^(string_of_list l)

let string_of_func (args,res) =
  "("^(string_of_list args)^") -> "^res


type 'a tree = Node of 'a * 'a tree list


(* let rec add_to_tree my_class parent_class classes_met tree = match tree with
	| Node(c, t_list) when c = parent_class -> Node(c, Node(my_class, [])::t_list); 
	| Node(c, t_list) when not (exists (function x -> x=parent_class) classes_met) ->
		let rec check_children c_met t_list = match t_list with 
		 	| [] -> []
		 	| h::t -> (add_to_tree my_class parent_class c_met h)::(check_children c_met t) in
		Node(c, check_children (c::classes_met) t_list);
	| _ -> circular_class_def my_class *)

