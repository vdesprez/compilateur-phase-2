open AST(* collect the parents' methods *)
open Error
open Located
open List

(* the types *)
type methodabstract =
    {
        mtype: Type.t;
        margs: (string * Type.t) list;
    }

type classabstract =
    {
        cparent: Type.t;
        cmethods: (string, methodabstract) Hashtbl.t;
        cattr: (string, Type.t) Hashtbl.t;
    }
        
type astabstract = (Type.t, classabstract) Hashtbl.t


let assert_empty_cell table loc = function
    name when not (Hashtbl.mem table name) -> ()
  | _ -> already_defined loc

let assert_not_in_list name loc mlist =
    begin try
        let res = assoc name mlist in already_defined loc
    with Not_found -> ()
    end

(* a [name, type] list of the objects in the context *)
type context = (string,  Type.t) Hashtbl.t


(*
 * populate the abstracts
 * *)

let populate_object_class pgm_struct =
    let methods = Hashtbl.create 0
    and attrs = Hashtbl.create 0
    in Hashtbl.add pgm_struct (Type.fromString "Object")
    {
        cparent = Type.fromString "Object";
        cmethods = methods;
        cattr = attrs;
    }

let populate_string_class pgm_struct =
    let methods = Hashtbl.create 10
    and args = [("rop", Type.fromString "String")]
    and attrs = Hashtbl.create 1
    in
        Hashtbl.add attrs "value" (Type.fromString "String");
        Hashtbl.add methods "print" { mtype = Type.fromString "String"; margs = []};
        Hashtbl.add methods "add" { mtype = Type.fromString "String"; margs = args };
        Hashtbl.add methods "eq" { mtype = Type.fromString "Boolean"; margs = args };
        Hashtbl.add methods "neq" { mtype = Type.fromString "Boolean"; margs = args };
        Hashtbl.add pgm_struct (Type.fromString "String")
        {
            cparent = Type.fromString "Object";
            cmethods = methods;
            cattr = attrs;
        }

let populate_int_class pgm_struct =
    let methods = Hashtbl.create 10
    and args = [("rop", Type.fromString "Int")]
    and attrs = Hashtbl.create 1
    in
        Hashtbl.add attrs "value" (Type.fromString "Int");
        Hashtbl.add methods "neg" { mtype = Type.fromString "Int"; margs = [] };

        Hashtbl.add methods "add" { mtype = Type.fromString "Int"; margs = args };
        Hashtbl.add methods "sub" { mtype = Type.fromString "Int"; margs = args };
        Hashtbl.add methods "mul" { mtype = Type.fromString "Int"; margs = args };
        Hashtbl.add methods "div" { mtype = Type.fromString "Int"; margs = args };
        Hashtbl.add methods "mod" { mtype = Type.fromString "Int"; margs = args };
        Hashtbl.add methods "gt" { mtype = Type.fromString "Boolean"; margs = args };
        Hashtbl.add methods "ge" { mtype = Type.fromString "Boolean"; margs = args };
        Hashtbl.add methods "lt" { mtype = Type.fromString "Boolean"; margs = args };
        Hashtbl.add methods "le" { mtype = Type.fromString "Boolean"; margs = args };
        Hashtbl.add methods "eq" { mtype = Type.fromString "Boolean"; margs = args };
        Hashtbl.add methods "neq" { mtype = Type.fromString "Boolean"; margs = args };
        Hashtbl.add pgm_struct (Type.fromString "Int")
        {
            cparent = Type.fromString "Object";
            cmethods = methods;
            cattr = attrs;
        }

let populate_boolean_class pgm_struct =
    let methods = Hashtbl.create 10
    and args = [("rop", Type.fromString "Boolean")]
    and attrs = Hashtbl.create 1
    in
        Hashtbl.add attrs "value" (Type.fromString "Boolean");
        Hashtbl.add methods "not" { mtype = Type.fromString "Boolean"; margs = [] };

        Hashtbl.add methods "eq" { mtype = Type.fromString "Boolean"; margs = args };
        Hashtbl.add methods "and" { mtype = Type.fromString "Boolean"; margs = args };
        Hashtbl.add methods "or" { mtype = Type.fromString "Boolean"; margs = args };
        Hashtbl.add methods "neq" { mtype = Type.fromString "Boolean"; margs = args };
        Hashtbl.add pgm_struct (Type.fromString "Boolean")
        {
            cparent = Type.fromString "Object";
            cmethods = methods;
            cattr = attrs;
        }

let populate_known_classes pgm_struct =
    populate_object_class pgm_struct;
    populate_string_class pgm_struct;
    populate_int_class pgm_struct;
    populate_boolean_class pgm_struct


(* 
Build the summaries 
*)
let rec make_attributes_summary attr_list attr_hash = match attr_list with
  | [] -> ()
  | h::t -> assert_empty_cell attr_hash h.aloc h.aname;
          Hashtbl.add attr_hash h.aname (elem_of h.atype); 
            make_attributes_summary t attr_hash

let make_args_list arglist loc =
    let rec populate_list = function
        [] -> []
      | (a, b)::tail ->  let other_args = populate_list tail in
                    assert_not_in_list a loc other_args;
                    (a, (elem_of b))::(other_args)
    in populate_list arglist


let rec make_method_summarys m_list m_hash = match m_list with
  | [] -> ()
  | h::t -> assert_empty_cell m_hash h.mloc h.mname;
    Hashtbl.add m_hash h.mname
    {
        mtype = elem_of h.mreturntype;
        margs = make_args_list h.margstype h.mloc;
    };
    make_method_summarys t m_hash


let rec make_class_summary class_list my_hash = match class_list with
  | [] -> ()
  | h::t -> assert_empty_cell my_hash h.cloc (Type.fromString h.cname);
        let attr_tbl = Hashtbl.create 900 and 
        methods_tbl = Hashtbl.create 900 in
        Hashtbl.add my_hash (Type.fromString h.cname)
        {
            cparent = elem_of h.cparent; 
            cattr = attr_tbl;
            cmethods = methods_tbl;
        } ;
        make_method_summarys h.cmethods methods_tbl ;
        make_attributes_summary h.cattributes attr_tbl;
        (make_class_summary t my_hash)

let make_ast_summary (class_list, e) = 
  let table = Hashtbl.create 9000 
  in populate_known_classes table; make_class_summary class_list table; table

let args_are_compatible ma1 ma2 =
    let rec compare_lists = function
        ([], []) -> true
      | (_, []) -> false
      | ([], _) -> false
      | (a1::t1, a2::t2) when snd a1 = snd a2 -> compare_lists (t1,t2)
      | _ -> false
    in compare_lists (ma1.margs, ma2.margs)

(* Add t2 to t1: t2 stays the same, new t1 = t1 U t2 *)
let add_methods t1 t2 = Hashtbl.iter 
  (fun a b -> 
    try 
      match (Hashtbl.find t1 a) with
        | found when args_are_compatible b found -> ()
        | _ -> bad_method_redefinition a
    with Not_found -> Hashtbl.add t1 a b
  ) 
  t2

(* Add t2 to t1: t2 stays the same, new t1 = t1 U t2 *)
let add_attributes t1 t2 = Hashtbl.iter 
  (fun a b -> 
    try 
      match (Hashtbl.find t1 a) with
        | found when b = found -> ()
        | _ -> bad_method_redefinition a
    with Not_found -> Hashtbl.add t1 a b
  ) 
  t2

(* collect parents' methods and check the signatures *)
let handle_inheritance summary_tbl = 
  let rec add_parents_methods cnom cval = match cval.cparent with
    | obj when Type.stringOf obj = "Object" -> ()
    | p -> 
      let parent = Hashtbl.find summary_tbl cval.cparent in
      add_parents_methods cnom parent;
      add_methods cval.cmethods parent.cmethods;
      add_attributes cval.cattr parent.cattr
  in
  Hashtbl.iter add_parents_methods summary_tbl


(* utility functions *)
let first_equals (a, d) = function
    (b, c) when b = a -> true
  | _ -> false

let from_option loc = function
    None -> empty_option loc
  | Some(a) -> a

let get getMEEEEEEEE = match getMEEEEEEEE with
  | None -> raise Not_found
  | Some(a) -> a

(* Operations on defined classes: inheritance, common ancester... *)
let type_matching e1 = function
    e2 when e1.etype = e2.etype -> true
  | _ -> typee e1.eloc

let rec find_classes l (astclasses, expr) = match astclasses with
  | [] -> l
  | c::d -> find_classes ((c.cname, elem_of c.cparent)::l) (d, expr)

let rec find_dup = function
    [] -> ()
  | a::tail when exists (first_equals a) tail -> duplicated_class (fst a)
  | a::tail -> find_dup tail

(* is_parent ast a b is true if b inherits from a or is a *)
let rec is_parent ast_summary a = function
    b when (compare b a) = 0 -> true
  | b when Type.stringOf b = "Object" -> false
  | b when (Hashtbl.find ast_summary b).cparent = a -> true
  | b -> is_parent ast_summary a ((Hashtbl.find ast_summary b).cparent)

let which_is_parent ast loc a = function
    b when is_parent ast a b -> a
  | b when is_parent ast b a -> b
  | _ -> types_not_matching loc

let assert_are_parents ast loc a = function
    b when is_parent ast a b -> ()
  | b when is_parent ast b a -> ()
  | _ -> types_not_matching loc

let assume_type_exists pgm_struct loc = function
    name when Hashtbl.mem pgm_struct name -> name
  | _ -> unknown_type loc

let assume_method_exists pgm_struct loc obj = function
    name when Hashtbl.mem pgm_struct name -> name
  | _ -> unknown_type loc


(* compare the types of the given list of args to the theoric ones *)
let right_args_types ast_summary method_summary loc = function
  | argslist -> let rec match_arg_types  = function
    | ([], []) -> true
    | ([a], []) -> unmatching_args a.eloc
    | ([], [a]) -> unmatching_args loc
    | (arg::arg_list, (nom, t)::tail) when is_parent ast_summary t (get arg.etype) -> 
        match_arg_types (arg_list, tail)
    | (arg::arg_list, (nom, t)::tail) -> unmatching_args arg.eloc
  in match_arg_types (argslist, method_summary.margs)

(* Check if the given type has the method. We assume the type is correct *)
let has_method typename methodname summary_tbl =
  Hashtbl.mem ((Hashtbl.find summary_tbl typename).cmethods) methodname

(* Properly type the ast elements *)
let rec type_the_args mcontext pgm_struct = function
    [] -> []
  | a::tail -> (type_the_expr mcontext pgm_struct a)::(type_the_args mcontext pgm_struct tail)

and type_the_expr mcontext pgm_struct expr = match expr.edesc with
    New(a) -> expr.etype <- (Some(assume_type_exists pgm_struct expr.eloc (elem_of a))) ; expr

  | Seq(e1, e2) -> let (te1, te2) = ((type_the_expr mcontext pgm_struct e1), (type_the_expr mcontext pgm_struct e2))
            in expr.etype <- te2.etype; expr

  | Call(e1, mname, args) ->
          let texpr = type_the_expr mcontext pgm_struct e1
          and targs = type_the_args mcontext pgm_struct args
          in begin try 
              (* does texpr know the method? *)
              let mmethod = Hashtbl.find ((Hashtbl.find pgm_struct (from_option
              texpr.eloc texpr.etype )).cmethods) mname
              (* are the arguments of the right types? *)
              in match (right_args_types pgm_struct mmethod expr.eloc targs) with
                true    -> expr.etype <- Some(mmethod.mtype) ; expr
                | _     -> unmatching_args expr.eloc
          with Not_found -> unknown_method expr.eloc
          end
 
  | If(cond, expr1, expr2)  -> let tuple = (type_the_expr mcontext pgm_struct cond,
                                   type_the_expr mcontext pgm_struct expr1,
                                   type_the_expr mcontext pgm_struct expr2) in
        begin match tuple with
        (* is the condition a boolean? *)
          (tcond, _, _)   when (compare (tcond.etype) (Some(Type.fromString "Boolean")) <> 0)
                                -> bad_cond_type(tcond.eloc)
        (* Get the common ancestor of the two expression's type *)
        | ( _, texpr1, texpr2)  ->
                expr.etype <- Some(which_is_parent pgm_struct expr.eloc 
                                (from_option texpr1.eloc texpr1.etype) (from_option texpr2.eloc texpr2.etype));
                expr
        end

  | Val v -> begin match v with
              String s  -> expr.etype <- Some(Type.fromString "String") ; expr
            | Boolean b -> expr.etype <- Some(Type.fromString "Boolean") ; expr
            | Int i     -> expr.etype <- Some(Type.fromString "Int"); expr
            | Null      -> expr.etype <- Some(Type.fromString "Object"); expr
        end

  | Var v -> begin try expr.etype <- Some(Hashtbl.find mcontext v); expr
            with Not_found -> unknown_var expr.eloc end

  | Assign(varname, aexpr) ->
          begin try
              let texpr = type_the_expr mcontext pgm_struct aexpr
              and tvar = Some(Hashtbl.find mcontext varname) in
              match texpr.etype with
              (* is expr a specialization of initial varname type? *)
              et when is_parent pgm_struct (get tvar)  (get et) -> expr.etype <- tvar; expr
            | _ -> types_not_matching expr.eloc
          with Not_found -> unknown_var expr.eloc
          end

(* there already is a so-named variable in the context *)
  | Define(name, _, _, _) when Hashtbl.mem mcontext name ->
          already_defined expr.eloc 
(* unknown type *)
  | Define(_, ldtype, _, _) when not (Hashtbl.mem pgm_struct (elem_of ldtype)) ->
          unknown_type expr.eloc 
  | Define(name, ldtype, einit, ebody) ->
          let teinit = type_the_expr mcontext pgm_struct einit
          and dtype = elem_of ldtype in
          begin match teinit.etype with
            et when is_parent pgm_struct (dtype) (get et) ->
                Hashtbl.add mcontext name dtype;
                type_the_expr mcontext pgm_struct ebody;
                Hashtbl.remove mcontext name;
                expr.etype <- Some(dtype);
                expr
          | _ -> types_not_matching teinit.eloc
          end

  | Cast(lctype, mexpr)
        -> let ctype = elem_of lctype
         and texpr = type_the_expr mcontext pgm_struct mexpr
         in assert_are_parents pgm_struct expr.eloc (from_option texpr.eloc
         texpr.etype) ctype; expr.etype <- Some(ctype); expr

  | Instanceof(iexpr, litype)
        when elem_of litype = assume_type_exists pgm_struct expr.eloc (elem_of litype) -> 
          expr.etype <- Some(Type.fromString "Boolean"); 
          type_the_expr mcontext pgm_struct iexpr;
          expr

(* type all the exprs in the method and check if the last expr is of the right
 * type *)
let rec copy_args_to_context table = function
    [] -> ()
  | (a, b)::tail -> Hashtbl.add table a (elem_of b); copy_args_to_context table tail

let copy_attrs_to_context mcontext attrs = 
  Hashtbl.iter (fun anom atype -> Hashtbl.add mcontext anom atype) attrs 

let check_method pgm_struct m obj_type =
    let mcontext = Hashtbl.create 10 in
    Hashtbl.add mcontext "this" obj_type;
    copy_args_to_context mcontext m.margstype;
    copy_attrs_to_context mcontext (Hashtbl.find pgm_struct obj_type).cattr;
    let res_expr = type_the_expr mcontext pgm_struct m.mbody in
    match m.mreturntype with
        rt when is_parent pgm_struct (elem_of rt) (get res_expr.etype) -> m
      | _ -> types_not_matching m.mloc

let rec check_methods pgm_struct obj_type = function
    [] -> ()
  | m::tail -> check_method pgm_struct m obj_type; check_methods pgm_struct obj_type tail

let check_attribute pgm_struct a obj_type=
    let mtype = assume_type_exists pgm_struct a.aloc (elem_of a.atype)
    and mcontext = Hashtbl.create 10 in
    Hashtbl.add mcontext "this" obj_type;
    match a.adefault with
    None -> ()
  | Some(expr) -> let res_type = (type_the_expr mcontext pgm_struct expr).etype in
                    begin match res_type with
                    Some(atype) when is_parent pgm_struct mtype atype
                            -> ()
                    | _ -> types_not_matching a.aloc
                    end

let rec check_attributes pgm_struct obj_type = function
    [] -> ()
  | a::tail -> check_attribute pgm_struct a obj_type;
                check_attributes pgm_struct obj_type tail

let check_types_in_class pgm_struct c =
    check_methods pgm_struct (Type.fromString c.cname) c.cmethods;
    check_attributes pgm_struct (Type.fromString c.cname) c.cattributes


let rec check_classes pgm_struct = function
    [] -> ()
  | c::tail -> check_types_in_class pgm_struct c;
                check_classes pgm_struct tail

let check_ast pgm_struct = function
    (clist, None) -> check_classes pgm_struct clist;
  | (clist, Some(expr)) -> check_classes pgm_struct clist; 
                    type_the_expr (Hashtbl.create 5) pgm_struct expr; ()




(* Main function of the file *)
let doTyping ast =
    let table = make_ast_summary ast in
   (*  let summary_without_inheritance = Hashtbl.copy table in *)
    handle_inheritance table;
    check_ast table ast; (ast, table)
